package org.apache.lucene.queryparser.flexible.standard.processors;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.CachingTokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.queryparser.flexible.core.QueryNodeException;
import org.apache.lucene.queryparser.flexible.core.config.QueryConfigHandler;
import org.apache.lucene.queryparser.flexible.core.nodes.FieldQueryNode;
import org.apache.lucene.queryparser.flexible.core.nodes.FuzzyQueryNode;
import org.apache.lucene.queryparser.flexible.core.nodes.GroupQueryNode;
import org.apache.lucene.queryparser.flexible.core.nodes.NoTokenFoundQueryNode;
import org.apache.lucene.queryparser.flexible.core.nodes.QueryNode;
import org.apache.lucene.queryparser.flexible.core.nodes.QuotedFieldQueryNode;
import org.apache.lucene.queryparser.flexible.core.nodes.RangeQueryNode;
import org.apache.lucene.queryparser.flexible.core.nodes.TextableQueryNode;
import org.apache.lucene.queryparser.flexible.core.nodes.TokenizedPhraseQueryNode;
import org.apache.lucene.queryparser.flexible.core.processors.QueryNodeProcessorImpl;
import org.apache.lucene.queryparser.flexible.standard.config.StandardQueryConfigHandler.ConfigurationKeys;
import org.apache.lucene.queryparser.flexible.standard.nodes.MultiPhraseQueryNode;
import org.apache.lucene.queryparser.flexible.standard.nodes.PrefixWildcardQueryNode;
import org.apache.lucene.queryparser.flexible.standard.nodes.RegexpQueryNode;
import org.apache.lucene.queryparser.flexible.standard.nodes.StandardBooleanQueryNode;
import org.apache.lucene.queryparser.flexible.standard.nodes.WildcardQueryNode;

/**
 * This processor verifies if {@link ConfigurationKeys#ANALYZER}
 * is defined in the {@link QueryConfigHandler}. If it is and the analyzer is
 * not <code>null</code>, it looks for every {@link FieldQueryNode} that is not
 * {@link WildcardQueryNode}, {@link FuzzyQueryNode} or
 * {@link RangeQueryNode} contained in the query node tree, then it applies
 * the analyzer to that {@link FieldQueryNode} object. <br/>
 * <br/>
 * If the analyzer return only one term, the returned term is set to the
 * {@link FieldQueryNode} and it's returned. <br/>
 * <br/>
 * If the analyzer return more than one term, a {@link TokenizedPhraseQueryNode}
 * or {@link MultiPhraseQueryNode} is created, whether there is one or more
 * terms at the same position, and it's returned. <br/>
 * <br/>
 * If no term is returned by the analyzer a {@link NoTokenFoundQueryNode} object
 * is returned. <br/>
 * 
 * @see ConfigurationKeys#ANALYZER
 * @see Analyzer
 * @see TokenStream
 */
public class AnalyzerQueryNodeProcessor extends QueryNodeProcessorImpl {

  private Analyzer analyzer;

  private boolean positionIncrementsEnabled;

  public AnalyzerQueryNodeProcessor() {
    // empty constructor
  }

  @Override
  public QueryNode process(QueryNode queryTree) throws QueryNodeException {
    Analyzer analyzer = getQueryConfigHandler().get(ConfigurationKeys.ANALYZER);
    
    if (analyzer != null) {
      this.analyzer = analyzer;
      this.positionIncrementsEnabled = false;
      Boolean positionIncrementsEnabled = getQueryConfigHandler().get(ConfigurationKeys.ENABLE_POSITION_INCREMENTS);

      if (positionIncrementsEnabled != null) {
          this.positionIncrementsEnabled = positionIncrementsEnabled;
      }

      if (this.analyzer != null) {
        return super.process(queryTree);
      }

    }

    return queryTree;

  }

  @Override
  protected QueryNode postProcessNode(QueryNode node) throws QueryNodeException {

    if (node.getParent() instanceof RangeQueryNode) {
      return node;
    }
    if (node instanceof PrefixWildcardQueryNode) {
      PrefixWildcardQueryNode prefixNode = (PrefixWildcardQueryNode) node;
      return postProcessPrefixWildcardQueryNode(prefixNode);
    }
    if (node instanceof WildcardQueryNode) {
      WildcardQueryNode wildcardNode = (WildcardQueryNode) node;
      return postProcessWildcardQueryNode(wildcardNode);
    }
    if (node instanceof FuzzyQueryNode) {
      FuzzyQueryNode fuzzyNode = (FuzzyQueryNode) node;
      return postProcessFuzzyQueryNode(fuzzyNode);
    }
    if (node instanceof FieldQueryNode) {
      FieldQueryNode fieldNode = (FieldQueryNode) node;
      return postProcessFieldQueryNode(fieldNode);
    }

    return node;
  }

  // gobble escaped chars or find a wildcard character
  private final Pattern wildcardPattern = Pattern.compile("(\\.)|([?*]+)");

  private QueryNode postProcessPrefixWildcardQueryNode(PrefixWildcardQueryNode prefixNode) throws QueryNodeException {
    String field = prefixNode.getFieldAsString();
    String termStr = prefixNode.getTextAsString();
    if (termStr == null) {
      throw new QueryNodeException(new IllegalArgumentException("PrefixWildcardQueryNode term is null."));
    }
    int termLen = termStr.length();
    if (termLen < 1) {
      throw new QueryNodeException(new IllegalArgumentException("PrefixWildcardQueryNode term is empty"));
    }
    String wildCard = termStr.substring(termLen - 1);
    String normalized = normalizeSingleChunk(field, termStr, termStr.substring(0, termLen - 1));
    return new PrefixWildcardQueryNode(field,
            normalized == null ? termStr : normalized + wildCard,
            prefixNode.getBegin(), prefixNode.getEnd());
  }

  private QueryNode postProcessWildcardQueryNode(WildcardQueryNode wildcardNode) throws QueryNodeException {
    String field = wildcardNode.getFieldAsString();
    String termStr = wildcardNode.getTextAsString();
    Matcher wildcardMatcher = wildcardPattern.matcher(termStr);
    StringBuilder sb = new StringBuilder();
    int last = 0;

    while (wildcardMatcher.find()) {
      // continue if escaped char
      if (wildcardMatcher.group(1) != null){
        continue;
      }

      if (wildcardMatcher.start() > 0) {
        String chunk = termStr.substring(last, wildcardMatcher.start());
        String normalized = normalizeSingleChunk(field, termStr, chunk);
        if (normalized == null) {
          // Unexpected term that cannot be normalized as a single token, return the original node
          return wildcardNode;
        }
        sb.append(normalized);
      }
      //append the wildcard character
      sb.append(wildcardMatcher.group(2));

      last = wildcardMatcher.end();
    }
    if (last < termStr.length()) {
      String normalized = normalizeSingleChunk(field, termStr, termStr.substring(last));
      if (normalized == null) {
        return wildcardNode;
      }
      sb.append(normalized);
    }
    return new WildcardQueryNode(field, sb.toString(), wildcardNode.getBegin(), wildcardNode.getEnd());
  }

  private QueryNode postProcessFuzzyQueryNode(FuzzyQueryNode fuzzyNode) throws QueryNodeException {
    String field = fuzzyNode.getFieldAsString();
    String termStr = fuzzyNode.getTextAsString();
    String normalized = normalizeSingleChunk(field, termStr, termStr);
    return new FuzzyQueryNode(field,
            normalized == null ? termStr : normalized, fuzzyNode.getSimilarity(),
            fuzzyNode.getBegin(), fuzzyNode.getEnd());
  }

  /**
   * Returns the normalized form for the given chunk
   *
   * If the analyzer produces more than one output token from the given chunk, the original term is returned.
   *
   * @param field The target field
   * @param termStr The full term from which the given chunk is excerpted
   * @param chunk The portion of the given termStr to be analyzed
   * @return The result of normalizing the given chunk, or <code>null</code> if the analyzer returns no tokens
   *  or more than one tokens.
   */
  protected String normalizeSingleChunk(String field, String termStr, String chunk) throws QueryNodeException {
    String normalized = null;
    TokenStream stream = null;
    try {
      stream = analyzer.normalizedTokenStream(field, chunk);
      stream.reset();
      CharTermAttribute termAtt = stream.getAttribute(CharTermAttribute.class);
      // get first and hopefully only output token
      if (stream.incrementToken()) {
        normalized = termAtt.toString();

        // try to increment again, there should only be one output token
        Boolean multipleOutputs = false;
        while (stream.incrementToken()) {
          multipleOutputs = true;
        }
        if (multipleOutputs) {
          // Analyzer created multiple terms for the term.
          return null;
        }
      } else {
        // nothing returned by analyzer, let's return the original term then.
        return null;
      }
    } catch (IOException e) {
      throw new QueryNodeException(e);
    } finally {
      if (stream != null) {
        try {
          stream.end();
          stream.close();
        } catch (IOException ignored) {
          // ignore
        }
      }
    }
    return normalized;
  }

  private QueryNode postProcessFieldQueryNode(FieldQueryNode fieldNode) throws QueryNodeException {
      String text = fieldNode.getTextAsString();
      String field = fieldNode.getFieldAsString();

      TokenStream source;
      try {
        source = this.analyzer.tokenStream(field, text);
        source.reset();
      } catch (IOException e1) {
        throw new RuntimeException(e1);
      }
      CachingTokenFilter buffer = new CachingTokenFilter(source);

      PositionIncrementAttribute posIncrAtt = null;
      int numTokens = 0;
      int positionCount = 0;
      boolean severalTokensAtSamePosition = false;

      if (buffer.hasAttribute(PositionIncrementAttribute.class)) {
        posIncrAtt = buffer.getAttribute(PositionIncrementAttribute.class);
      }

      try {

        while (buffer.incrementToken()) {
          numTokens++;
          int positionIncrement = (posIncrAtt != null) ? posIncrAtt
              .getPositionIncrement() : 1;
          if (positionIncrement != 0) {
            positionCount += positionIncrement;

          } else {
            severalTokensAtSamePosition = true;
          }

        }

      } catch (IOException e) {
        // ignore
      }

      try {
        // rewind the buffer stream
        buffer.reset();

        // close original stream - all tokens buffered
        source.close();
      } catch (IOException e) {
        // ignore
      }

      if (!buffer.hasAttribute(CharTermAttribute.class)) {
        return new NoTokenFoundQueryNode();
      }

      CharTermAttribute termAtt = buffer.getAttribute(CharTermAttribute.class);

      if (numTokens == 0) {
        return new NoTokenFoundQueryNode();

      } else if (numTokens == 1) {
        String term = null;
        try {
          boolean hasNext;
          hasNext = buffer.incrementToken();
          assert hasNext == true;
          term = termAtt.toString();

        } catch (IOException e) {
          // safe to ignore, because we know the number of tokens
        }

        fieldNode.setText(term);

        return fieldNode;

      } else if (severalTokensAtSamePosition || !(fieldNode instanceof QuotedFieldQueryNode)) {
        if (positionCount == 1 || !(fieldNode instanceof QuotedFieldQueryNode)) {
          // no phrase query:
          LinkedList<QueryNode> children = new LinkedList<QueryNode>();

          for (int i = 0; i < numTokens; i++) {
            String term = null;
            try {
              boolean hasNext = buffer.incrementToken();
              assert hasNext == true;
              term = termAtt.toString();

            } catch (IOException e) {
              // safe to ignore, because we know the number of tokens
            }

            children.add(new FieldQueryNode(field, term, -1, -1));

          }
          return new GroupQueryNode(
            new StandardBooleanQueryNode(children, positionCount==1));
        } else {
          // phrase query:
          MultiPhraseQueryNode mpq = new MultiPhraseQueryNode();

          List<FieldQueryNode> multiTerms = new ArrayList<FieldQueryNode>();
          int position = -1;
          int i = 0;
          int termGroupCount = 0;
          for (; i < numTokens; i++) {
            String term = null;
            int positionIncrement = 1;
            try {
              boolean hasNext = buffer.incrementToken();
              assert hasNext == true;
              term = termAtt.toString();
              if (posIncrAtt != null) {
                positionIncrement = posIncrAtt.getPositionIncrement();
              }

            } catch (IOException e) {
              // safe to ignore, because we know the number of tokens
            }

            if (positionIncrement > 0 && multiTerms.size() > 0) {

              for (FieldQueryNode termNode : multiTerms) {

                if (this.positionIncrementsEnabled) {
                  termNode.setPositionIncrement(position);
                } else {
                  termNode.setPositionIncrement(termGroupCount);
                }

                mpq.add(termNode);

              }

              // Only increment once for each "group" of
              // terms that were in the same position:
              termGroupCount++;

              multiTerms.clear();

            }

            position += positionIncrement;
            multiTerms.add(new FieldQueryNode(field, term, -1, -1));

          }

          for (FieldQueryNode termNode : multiTerms) {

            if (this.positionIncrementsEnabled) {
              termNode.setPositionIncrement(position);

            } else {
              termNode.setPositionIncrement(termGroupCount);
            }

            mpq.add(termNode);

          }

          return mpq;

        }

      } else {

        TokenizedPhraseQueryNode pq = new TokenizedPhraseQueryNode();

        int position = -1;

        for (int i = 0; i < numTokens; i++) {
          String term = null;
          int positionIncrement = 1;

          try {
            boolean hasNext = buffer.incrementToken();
            assert hasNext == true;
            term = termAtt.toString();

            if (posIncrAtt != null) {
              positionIncrement = posIncrAtt.getPositionIncrement();
            }

          } catch (IOException e) {
            // safe to ignore, because we know the number of tokens
          }

          FieldQueryNode newFieldNode = new FieldQueryNode(field, term, -1, -1);

          if (this.positionIncrementsEnabled) {
            position += positionIncrement;
            newFieldNode.setPositionIncrement(position);

          } else {
            newFieldNode.setPositionIncrement(i);
          }

          pq.add(newFieldNode);

        }

        return pq;

      }

  }

  @Override
  protected QueryNode preProcessNode(QueryNode node) throws QueryNodeException {

    return node;

  }

  @Override
  protected List<QueryNode> setChildrenOrder(List<QueryNode> children)
      throws QueryNodeException {

    return children;

  }

}
