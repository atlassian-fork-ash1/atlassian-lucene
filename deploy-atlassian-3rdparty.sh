#!/bin/bash

export ANT_OPTS=-Xmx1g
repository_id=atlassian-3rdparty
repository_url=https://maven.atlassian.com/3rdparty

version=$1
ant clean remove-maven-artifacts
ant -Dversion=$version generate-maven-artifacts

projects=`ls -la dist/maven/org/apache/lucene | grep 'lucene-' | cut -d ' ' -f14`
for project in $projects
do
	echo "Deploying project $project"
	mvn deploy:deploy-file -Durl=$repository_url -DrepositoryId=$repository_id -Dfile=dist/maven/org/apache/lucene/$project/$version/$project-$version.jar -DgroupId=org.apache.lucene -DartifactId=$project -Dversion=$version -Dpackaging=jar -DpomFile=dist/maven/org/apache/lucene/$project/$version/$project-$version.pom -Dsources=dist/maven/org/apache/lucene/$project/$version/$project-$version-sources.jar
	echo ""
done

mvn deploy:deploy-file -Durl=https://maven.atlassian.com/3rdparty -DrepositoryId=atlassian-3rdparty -DgroupId=org.apache.lucene -DartifactId=lucene-parent -Dversion=$version -Dpackaging=pom -Dfile=dist/maven/org/apache/lucene/lucene-parent/$version/lucene-parent-$version.pom -DpomFile=dist/maven/org/apache/lucene/lucene-parent/$version/lucene-parent-$version.pom

mvn deploy:deploy-file -Durl=https://maven.atlassian.com/3rdparty -DrepositoryId=atlassian-3rdparty -DgroupId=org.apache.lucene -DartifactId=lucene-solr-grandparent -Dversion=$version -Dpackaging=pom -Dfile=dist/maven/org/apache/lucene/lucene-solr-grandparent/$version/lucene-solr-grandparent-$version.pom -DpomFile=dist/maven/org/apache/lucene/lucene-solr-grandparent/$version/lucene-solr-grandparent-$version.pom